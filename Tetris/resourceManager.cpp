#include "resourceManager.h"

resourceManager::resourceManager()
{
}

resourceManager::~resourceManager()
{
}

resourceManager& resourceManager::graphicsInit()
{
	background_t.loadFromFile("resources/background.png");
	colors_t.loadFromFile("resources/sqrs.png");

	background_s.setTexture(background_t);

	return *this;
}

resourceManager& resourceManager::soundInit()
{
	soundBuffers[Sounds::BLOCK].loadFromFile("resources/block.wav");
	soundBuffers[Sounds::LINE_1].loadFromFile("resources/explosion1.wav");
	soundBuffers[Sounds::LINE_2].loadFromFile("resources/explosion2.wav");
	soundBuffers[Sounds::LINE_3].loadFromFile("resources/explosion3.wav");
	soundBuffers[Sounds::LINE_4].loadFromFile("resources/explosion4.wav");
	soundBuffers[Sounds::GAMEOVER].loadFromFile("resources/lose.ogg");

	for (auto const& buffer : soundBuffers) {
		gameSounds.emplace_back(buffer);
	}

	return *this;
}

resourceManager& resourceManager::musicInit()
{
	music.openFromFile("resources/Tetris.ogg");
	return *this;
}

resourceManager & resourceManager::fontInit()
{
	font.loadFromFile("resources/PressStart2P.ttf");
	return *this;
}

sf::Sprite& resourceManager::getColorSprite(int color)
{
	sf::Sprite temp(colors_t, sf::IntRect((color -1) * 20, 0, 20, 20));
	return temp;
}

sf::Sprite& resourceManager::getBackground()
{
	return background_s;
}

sf::Music* resourceManager::getMusic()
{
	return &music;
}

sf::Sound* resourceManager::getSound(Sounds sound)
{
	return &gameSounds[sound];
}

sf::Font*  resourceManager::getFont()
{
	return &font;
}
