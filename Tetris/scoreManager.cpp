#include "scoreManager.h"
 
scoreManager::scoreManager()
{
}


scoreManager::~scoreManager()
{
}

void scoreManager::Init(resourceManager *resources, sf::RenderWindow *window)
{
	resourcesReference = resources;
	windowReference = window;
}

float scoreManager::getDelay() const
{
	return (0.05 * (11 - level));
}

void scoreManager::updateLevel(int lines)
{
	linesCleared += lines;
	if ((linesCleared >= 1) && (linesCleared <= 90))
	{
		level = 1 + ((linesCleared - 1) / 10);
	}
	else if (linesCleared >= 91)
	{
		level = 10;
	}
}

void scoreManager::updateFreefallScore(int freefall)
{
	currentScore += ((21 + (3 * level)) - freefall);
}

void scoreManager::updateLinesClearedScore(int lines)
{
	updateLevel(lines);
	currentScore += lineMultiplier[lines - 1] * level;
}

void scoreManager::textConfig(sf::Text& text) {
	sf::Font *font = resourcesReference->getFont();
	text.setFont(*font);
	text.setCharacterSize(16);
	text.setFillColor(sf::Color::Black);
}

void scoreManager::drawText(const std::string & str, const int& x, const int& y, const unsigned int & style)
{
	sf::Text tempText;
	textConfig(tempText);
	tempText.setStyle(style);
	tempText.setString(str);
	tempText.setPosition(x, y);
	windowReference->draw(tempText);
}

void scoreManager::draw()
{	
	drawText("Score", 230, 20, sf::Text::Bold | sf::Text::Underlined);
	drawText(std::to_string(currentScore), 230, 40, sf::Text::Bold);
	drawText("Level", 230, 70, sf::Text::Bold | sf::Text::Underlined);
	drawText(std::to_string(level), 230, 90, sf::Text::Bold);
}

void scoreManager::reset()
{
	linesCleared = 0;
	level = 1;
	currentScore = 0;
}
