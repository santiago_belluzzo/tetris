#pragma once

#include <unordered_map>
#include <SFML/Graphics.hpp>
#include <functional>
typedef void(*_function)(void);

class eventHandler
{

	std::unordered_map<unsigned int, std::function<void (void)>> eventMap;
public:
	eventHandler();
	~eventHandler();
	void registerEvent(const unsigned int eventCode, const std::function<void(void)> function);
	void handleEvent(const unsigned int eventCode);
};

