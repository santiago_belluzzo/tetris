#pragma once
#include <SFML/Graphics.hpp>
#include "resourceManager.h"

class scoreManager
{
	static constexpr int lineMultiplier[4] = { 40, 100, 300, 1200 };
	
	resourceManager *resourcesReference = nullptr;
	sf::RenderWindow *windowReference = nullptr;

	int currentScore = 0,
		level = 1,
		linesCleared = 0;
public:
	scoreManager();
	~scoreManager();
	void Init(resourceManager *ref, sf::RenderWindow *window);
	float getDelay() const;
	void updateFreefallScore(int freefall);
	void updateLinesClearedScore(int lines);
	void draw();
	void reset();
private:
	void updateLevel(int lines);
	void textConfig(sf::Text& text);
	void drawText(const std::string & str, const int& x, const int& y, const unsigned int & style);
};