// Tetris.cpp : Defines the entry point for the console application.
//

#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>
#include <time.h>

#include "resourceManager.h"
#include "gameEngine.h"
#include "Tetris.h"


int main()
{
	sf::RenderWindow window(sf::VideoMode(320, 480), "Tetris", sf::Style::Close | sf::Style::Titlebar);
	gameEngine game(&window);
	
	while (window.isOpen()) {

		sf::Event e;

		while (window.pollEvent(e)) {
			if (e.type == sf::Event::Closed) {
				window.close();
			}
			if (e.type == sf::Event::KeyPressed) {
				game.handleInput(e.key.code);
			}
		}

		game.run();
	}
	return 0;
}

