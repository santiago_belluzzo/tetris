#include "pieces.h"
#include <algorithm>


Piece::Piece()
{
	piecesArray[PiecesEnum::L] = {
		{ 0, 0, 0, 0, 0 },
		{ 0, 0, 1, 0, 0 },
		{ 0, 0, 1, 0, 0 },
		{ 0, 0, 1, 1, 0 },
		{ 0, 0, 0, 0, 0 }
	};
	piecesArray[PiecesEnum::J] = {
		{ 0, 0, 0, 0, 0 },
		{ 0, 0, 2, 0, 0 },
		{ 0, 0, 2, 0, 0 },
		{ 0, 2, 2, 0, 0 },
		{ 0, 0, 0, 0, 0 }
	};
	piecesArray[PiecesEnum::I] = {
		{ 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0 },
		{ 0, 3, 3, 3, 3 },
		{ 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0 }
	};
	piecesArray[PiecesEnum::SQR] = {
		{ 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0 },
		{ 0, 0, 4, 4, 0 },
		{ 0, 0, 4, 4, 0 },
		{ 0, 0, 0, 0, 0 }
	};
	piecesArray[PiecesEnum::N] = {
		{ 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 5, 0 },
		{ 0, 0, 5, 5, 0 },
		{ 0, 0, 5, 0, 0 },
		{ 0, 0, 0, 0, 0 }
	};
	piecesArray[PiecesEnum::_N] = {
		{ 0, 0, 0, 0, 0 },
		{ 0, 0, 6, 0, 0 },
		{ 0, 0, 6, 6, 0 },
		{ 0, 0, 0, 6, 0 },
		{ 0, 0, 0, 0, 0 }
	};
	piecesArray[PiecesEnum::T] = {
		{ 0, 0, 0, 0, 0 },
		{ 0, 0, 7, 0, 0 },
		{ 0, 0, 7, 7, 0 },
		{ 0, 0, 7, 0, 0 },
		{ 0, 0, 0, 0, 0 }
	};
}


Piece::~Piece()
{
}

pieceArray Piece::getPiece(int i) const
{
	return piecesArray[i];
}

pieceArray Piece::rotate(pieceArray pieceToRotate)
{
	pieceArray rotatedPiece(5, std::vector<uint8_t>(5, 0));
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 5; j++) {

			int xp = i - rotationX;
			int yp = j - rotationY;

			int x_rotated = -yp + rotationY;
			int y_rotated = xp + rotationX;

			rotatedPiece[x_rotated][y_rotated] = pieceToRotate[i][j];

		}
	}
	return rotatedPiece;
}

int Piece::getOffset(const pieceArray& piece) const
{
	int offset = 0;
	for (auto const& row : piece)
		if (std::all_of(row.begin(), row.end(), [](const int& i) { return i == 0; }))offset++;
		else break;
	return offset;
}
