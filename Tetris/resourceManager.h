#pragma once
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>
#include <memory>

enum Color { BROWN = 1, MAROON, RED, GREEN, CYAN, BLUE, PINK };
enum Sounds {BLOCK, LINE_1, LINE_2, LINE_3, LINE_4, GAMEOVER, SOUNDS_COUNT};

class resourceManager
{
	sf::Sprite background_s;

	sf::Texture background_t,
				colors_t;

	sf::Music music;

	std::vector<sf::SoundBuffer> soundBuffers = std::vector<sf::SoundBuffer>(SOUNDS_COUNT);
	std::vector<sf::Sound> gameSounds;

	sf::Font font;

public:
	resourceManager();
	~resourceManager();
	resourceManager& graphicsInit();
	resourceManager& soundInit();
	resourceManager& musicInit();
	resourceManager& fontInit();
	sf::Sprite& getColorSprite(int color);
	sf::Sprite& getBackground();
	sf::Music* getMusic() ;
	sf::Sound* getSound(Sounds sound);
	sf::Font* getFont();
};

