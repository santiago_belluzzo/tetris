#pragma once
#include <SFML/Graphics.hpp>
#include <algorithm>
#include <vector>

enum  PiecesEnum { L = 0, J, I, SQR, N, _N, T, PIECES_COUNT};

typedef std::vector<std::vector<uint8_t> > pieceArray;

class Piece
{
	std::vector<pieceArray> piecesArray = std::vector<pieceArray>(PIECES_COUNT);

	static const uint8_t rotationX = 2, rotationY = 2;

public:
	Piece();
	~Piece();
	pieceArray getPiece(int i) const;
	pieceArray rotate(pieceArray pieceToRotate);
	int getOffset(const pieceArray& piece) const;
};