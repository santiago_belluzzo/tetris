#include "Jukebox.h"


Jukebox::Jukebox()
{
}


Jukebox::~Jukebox()
{
}

void Jukebox::Init(resourceManager  *resourcesReference)
{
	resources = resourcesReference;
	gameMusic = resources->getMusic();
}

void Jukebox::playGameOverSound()
{
	if (!gameOverSoundPlayed) {
		playSound(Sounds::GAMEOVER);
		gameOverSoundPlayed = true;
	}
}

void Jukebox::musicPlay()
{
	gameMusic->setLoop(true);
	gameMusic->setVolume(10.f);
	gameMusic->play();
}

void Jukebox::gameOver()
{
	gameMusic->stop();
	playGameOverSound();
}

void Jukebox::lowerMusicVolume()
{
	if (gameMusic->getVolume() > 0.f)
		gameMusic->setVolume(gameMusic->getVolume() - 10.f);
}

void Jukebox::increaseMusicVolume()
{
	if (gameMusic->getVolume() < 100.f)
		gameMusic->setVolume(gameMusic->getVolume() + 10.f);
}

void Jukebox::lowerFxVolume()
{
	if (fxVolume > 0.f)
		fxVolume -= 10.f;
}

void Jukebox::increaseFxVolume()
{
	if (fxVolume < 100.f)
		fxVolume += 10.f;
}

void Jukebox::muteFx()
{
	if (fxMuted == true) {
		fxVolume = 50.f;
		fxMuted = false;
	}
	else {
		fxVolume = 0.f;
		fxMuted = true;
	}
}

void Jukebox::muteMusic()
{
	if (gameMusic->getStatus() != sf::Music::Status::Paused)
		gameMusic->pause();
	else
		gameMusic->play();
}

void Jukebox::playSound(Sounds snd)
{
	auto tmp = resources->getSound(snd);
	tmp->setVolume(fxVolume);
	tmp->play();
}
