#include "gameEngine.h"
#include <algorithm>
#include <memory>


gameEngine::gameEngine(sf::RenderWindow *window) : renderWindowReference(window)
{
	srand(time(0));
	resources.graphicsInit().musicInit().soundInit().fontInit();
	jukebox.Init(&resources);
	score.Init(&resources, renderWindowReference);
	jukebox.musicPlay();
	eventSetup();
	delay = score.getDelay();
}

void gameEngine::eventSetup()
{
	inputEvents.registerEvent(sf::Keyboard::Left, [&]() {if (!paused)moveLeft(); });
	inputEvents.registerEvent(sf::Keyboard::Right, [&]() {if (!paused)moveRigth(); });
	inputEvents.registerEvent(sf::Keyboard::Up, [&]() {if (!paused)rotateCurrentPiece(); });
	inputEvents.registerEvent(sf::Keyboard::R, [&]() {if (gameOver())reset(); });
	inputEvents.registerEvent(sf::Keyboard::P, [&]() {paused = (paused == true) ? false : true; });
	inputEvents.registerEvent(sf::Keyboard::F2, [&]() { jukebox.lowerMusicVolume(); });
	inputEvents.registerEvent(sf::Keyboard::F3, [&]() { jukebox.increaseMusicVolume(); });
	inputEvents.registerEvent(sf::Keyboard::F4, [&]() { jukebox.muteMusic(); });
	inputEvents.registerEvent(sf::Keyboard::F5, [&]() { jukebox.lowerFxVolume(); });
	inputEvents.registerEvent(sf::Keyboard::F6, [&]() { jukebox.increaseFxVolume(); });
	inputEvents.registerEvent(sf::Keyboard::F7, [&]() { jukebox.muteFx(); });

}

gameEngine::~gameEngine()
{
}

void gameEngine::getRandomPiece()
{
	currentPiece = pieces.getPiece(rand() % PiecesEnum::PIECES_COUNT);
	getNewPiece = false;
	currentPieceDx = startDx;
	currentPieceDy = - pieces.getOffset(currentPiece);
	freefallpenalty = 0;
}

void gameEngine::tick()
{
	freefallpenalty++;
	currentPieceDy++;

	if (checkColision()) {
		currentPieceDy--;
		fillBoard();
		if (!gameOver()) {
			score.updateFreefallScore(freefallpenalty);
			jukebox.playSound(Sounds::BLOCK);
		}
		checkLines();
		getNewPiece = true;
	}
}

void gameEngine::fillBoard()
{
	for (int i = 0; i < piece_height; i++) 
		for (int j = 0; j < piece_width; j++) 
			if (currentPiece[i][j] != 0) 
				board[i + currentPieceDy][j + currentPieceDx] = currentPiece[i][j];
}

bool gameEngine::checkColision() const
{
	for (int i = 0; i < piece_width; i++) 
		for (int j = 0; j < piece_height; j++) 
			if (currentPiece[i][j] != 0 && ((j + currentPieceDx) < 0 || (j + currentPieceDx) >= board_width || (i + currentPieceDy) >= board_height || (i + currentPieceDy < 0)))return true;
			else if (currentPiece[i][j] != 0 && board[i + currentPieceDy][j + currentPieceDx] != 0)return true;

	return false;
}

void gameEngine::checkLines()
{
	int LineToErase = 0, linesErased = 0;
	for (auto& lines : board) {
		if (std::all_of(lines.begin(), lines.end(), [](const int& x) {return x != 0; })) {
			board.erase(board.begin() + LineToErase);
			board.insert(board.begin(), std::vector<uint8_t>(board_width, 0));
			linesErased++;
		}
		LineToErase++;
	}
	if (linesErased != 0) {
		jukebox.playSound((Sounds)linesErased);
		score.updateLinesClearedScore(linesErased);
	}
}

void gameEngine::reset()
{
	for (auto & line : board)
		std::fill(line.begin(), line.end(), 0);
	jukebox.musicPlay();
	jukebox.gameOverSoundPlayed = false;
	score.reset();
}


void gameEngine::handleInput(unsigned int key)
{
	inputEvents.handleEvent(key);
}

template<typename T>
void gameEngine::drawMatrix(const std::vector<std::vector<T>> & matrix, const int xDisplacement, const int yDisplacement)
{
	static const int boardOffsetX = 24, boardOffsetY = 14, blockWidth = 20, blockHeight = 20;
	sf::Sprite tempSprite;
	for (int i = 0; i < matrix.size(); i++) {
		for (int j = 0; j < matrix[i].size(); j++) {
			if (matrix[i][j] != 0) {
				tempSprite = resources.getColorSprite(matrix[i][j]);
				tempSprite.setPosition((j + xDisplacement) * blockWidth, (i + yDisplacement) * blockHeight);
				tempSprite.move(boardOffsetX, boardOffsetY);
				renderWindowReference->draw(tempSprite);
			}
		}
	}
}

void gameEngine::draw()
{
	renderWindowReference->clear(sf::Color::Black);
	renderWindowReference->draw(resources.getBackground());
	
	drawMatrix(board, 0, 0);
	drawMatrix(currentPiece, currentPieceDx, currentPieceDy);
	score.draw();

	renderWindowReference->display();
}

void gameEngine::moveLeft()
{
	currentPieceDx --;
	if (checkColision())
		currentPieceDx ++;
}

void gameEngine::moveRigth()
{
	currentPieceDx ++;
	if (checkColision())
		currentPieceDx --;
}

void gameEngine::rotateCurrentPiece()
{
	auto previous = currentPiece;
	currentPiece = pieces.rotate(currentPiece);
	if (checkColision()) {
		currentPiece = previous;
	}
}

bool gameEngine::gameOver()
{
	if (std::any_of(std::begin(board[0]), std::end(board[0]), [](int x) {return x != 0; })) {
		jukebox.gameOver();
		return true;
	}
	return false;
}

void gameEngine::run() 
{
	if (!paused)
	{
		if (!gameOver() && getNewPiece == true)getRandomPiece();

		timer += clock.restart().asSeconds();

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))	delay = 0.05f;

		if (timer > delay) {
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) freefallpenalty--;
			tick();
			timer = 0;
		}
		delay = score.getDelay();
	}
	draw();
}