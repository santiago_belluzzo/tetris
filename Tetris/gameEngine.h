#pragma once
#include <stdlib.h>
#include <time.h> 
#include <vector>
#include <SFML\Graphics.hpp>

#include "pieces.h"
#include "resourceManager.h"
#include "Jukebox.h"
#include "scoreManager.h"
#include "eventHandler.h"

class gameEngine
{
	static const uint8_t board_width = 10,
		board_height = 20,
		piece_width = 5,
		piece_height = 5,
		startDx = 2;

	sf::RenderWindow *renderWindowReference = nullptr;

	pieceArray board = pieceArray(board_height, std::vector<uint8_t>(board_width, 0));

	Piece pieces;
	pieceArray currentPiece;
	int currentPieceDx = startDx,
		currentPieceDy = 0,
		freefallpenalty = 0;

	resourceManager resources;
	Jukebox jukebox;
	scoreManager score;
	eventHandler inputEvents;

	bool getNewPiece = true,
		paused = false;

	float timer = 0, delay = 0;
	sf::Clock clock;

public:
	gameEngine(sf::RenderWindow *window);
	~gameEngine();
	void getRandomPiece();
	void tick();
	void fillBoard();
	void draw();
	void moveLeft();
	void moveRigth();
	void rotateCurrentPiece();
	bool gameOver();
	void checkLines();
	void reset();
	void handleInput(unsigned int key);
	void run();

private:
	bool checkColision() const;
	template <typename T>
	void drawMatrix(const std::vector<std::vector<T>> & matrix, const int xDisplacement, const int yDisplacement);
	void eventSetup();

};

