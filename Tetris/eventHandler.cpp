#include "eventHandler.h"


eventHandler::eventHandler()
{
}


eventHandler::~eventHandler()
{
}

void eventHandler::registerEvent(unsigned int eventCode, std::function<void(void)> function)
{
	eventMap.emplace(eventCode, function);
}

void eventHandler::handleEvent(unsigned int eventCode)
{
	auto iter = eventMap.find(eventCode);
	if (iter == eventMap.end())return;
	(iter->second)();
}
