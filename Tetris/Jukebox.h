#pragma once
#include "resourceManager.h"

class Jukebox
{
	friend class gameEngine;
	resourceManager *resources = nullptr;

	sf::Music *gameMusic = nullptr;

	bool gameOverSoundPlayed = false,
		 fxMuted = false;

	float fxVolume = 50.f;
public:
	Jukebox();
	~Jukebox();
	void Init(resourceManager *resourcesReference);
	void playGameOverSound();
	void musicPlay();
	void gameOver();
	void lowerMusicVolume();
	void increaseMusicVolume();
	void lowerFxVolume();
	void increaseFxVolume();
	void muteFx();
	void muteMusic();
	void playSound(Sounds snd);
};

